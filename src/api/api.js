import request from '../utils/request'

export const IMPORT_URL = process.env.VUE_APP_BASE_API + '/shop/goodsTemp/import'

export const loginProvider = {
  login: (params) =>
    request({
      url: '/renren-admin/login',
      method: 'post',
      data: params
    }),
  logout: (params) =>
    request({
      url: '/renren-admin/logout',
      method: 'post',
      data: params
    })
}

export const qpcrparaProvider = {
  deleteById: (params) =>
    request({
      url: '/renren-admin/qpcr/qpcrpara/deleteById',
      method: 'post',
      data: params
    }),
  get: (params) =>
    request({
      url: '/renren-admin/qpcr/qpcrpara/get',
      method: 'post',
      data: params
    }),
  list: (params) =>
    request({
      url: '/renren-admin/qpcr/qpcrpara/list',
      method: 'post',
      data: params
    }),
  save: (params) =>
    request({
      url: '/renren-admin/qpcr/qpcrpara/save',
      method: 'post',
      data: params
    })
}

export const qpcrprogProvider = {
  deleteById: (params) =>
    request({
      url: '/renren-admin/qpcr/qpcrprog/deleteById',
      method: 'post',
      data: params
    }),
  get: (params) =>
    request({
      url: '/renren-admin/qpcr/qpcrprog/get',
      method: 'post',
      data: params
    }),
  page: (params) =>
    request({
      url: '/renren-admin/qpcr/qpcrprog/page',
      method: 'post',
      data: params
    }),
  save: (params) =>
    request({
      url: '/renren-admin/qpcr/qpcrprog/save',
      method: 'post',
      data: params
    })
}

export const qpcrreportProvider = {
  deleteById: (params) =>
    request({
      url: '/renren-admin/qpcr/qpcrreport/deleteById',
      method: 'post',
      data: params
    }),
  get: (params) =>
    request({
      url: '/renren-admin/qpcr/qpcrreport/get',
      method: 'post',
      data: params
    }),
  page: (params) =>
    request({
      url: '/renren-admin/qpcr/qpcrreport/page',
      method: 'post',
      data: params
    }),
  save: (params) =>
    request({
      url: '/renren-admin/qpcr/qpcrreport/save',
      method: 'post',
      data: params
    }),
  data: (params) =>
    request({
      url: '/renren-admin/qpcr/qpcrreport/data',
      method: 'post',
      data: params
    }),
  download: (params) =>
    request({
      url: '/renren-admin/qpcr/qpcrreport/download',
      method: 'post',
      data: params
    }),
  curve: (params) =>
    request({
      url: '/renren-admin/qpcr/qpcrreport/curve',
      method: 'post',
      data: params
    })
}
